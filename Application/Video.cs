﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces;
using Serilog;

namespace Application
{
    public class Video : IVideo
    {
        private IServiceVideo servicevideo;
        private List<string> arquivos;

        public Video(IServiceVideo videoService)
        {
            servicevideo = videoService;
        }

        public async Task ProcessarVideoAsync()
        {
            try
            {
                //carrega e trata os arquivos de video e retornas as SKUS
                Log.Information("Carregando arquivos de videos");
                arquivos = servicevideo.CarregaArquivos();
                Log.Information(arquivos.Count().ToString() + " arquivos carregados");

                //Envia para S3 AWS
                Log.Information("Enviando arquivos para S3");
                await servicevideo.EnviarVideoAsync(arquivos);

                //Deleta arquivo da pasta
                Log.Information("Deletando arquivos");
                servicevideo.DeletarArquivos(arquivos);

            }
            catch(Exception e)
            {
                Log.Error(e.Message);
            }
        }
    }
}
