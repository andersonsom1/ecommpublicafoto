﻿using Amazon.S3;
using Amazon.S3.Model;
using Domain;
using Domain.Interfaces;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Services
{
    public class ServiceVideo : IServiceVideo
    {
        private string urlbase;
        private string urlS3;
        private string accesskey;
        private string secretkey;
        private string bucket;
        private string directoryS3;

        public ServiceVideo(IOptions<AppSettings> appSettings)
        {
            urlbase = appSettings.Value.Url;
            urlS3 = appSettings.Value.UrlS3;
            accesskey = appSettings.Value.AccessKeyvideo;
            secretkey = appSettings.Value.SecretKey;
            bucket = appSettings.Value.Bucketnamevideo;
            directoryS3 = appSettings.Value.DirectoryS3;
        }

        public List<string> CarregaArquivos()
        {
            try
            {
                List<string> arqvideo = new List<string>();
                //carrega os videos
                foreach (string filevid in Directory.EnumerateFiles(urlbase))
                {
                    //valida se o arquivo é MP4
                    if (!filevid.Substring(filevid.LastIndexOf(".") + 1).ToUpper().Equals("MP4"))
                    {
                        File.Delete(filevid);
                        Log.Information("Arquivo : " + filevid.Substring(filevid.LastIndexOf("\\") + 1) + " deletado porque não é MP4");
                    }
                    else
                    {
                        arqvideo.Add(filevid);
                        Log.Information("Arquivo : " + filevid.Substring(filevid.LastIndexOf("\\") + 1) + " carregado com sucesso");
                    }
                }
                return arqvideo;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public void DeletarArquivos(List<string> arquivos)
        {
            try
            {
                foreach(string fileid in arquivos)
                {
                    File.Delete(fileid);
                    Log.Information("Arquivo : " + fileid.Substring(fileid.LastIndexOf("\\") + 1) + " deletado com sucesso");
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task EnviarVideoAsync(List<string> videos)
        {
            try
            {
                foreach (string arqvideo in videos)
                {
                    AmazonS3Config config = new AmazonS3Config();
                    config.ServiceURL = urlS3;
                    AmazonS3Client s3Client = new AmazonS3Client(accesskey, secretkey, config);
                    PutObjectRequest request = new PutObjectRequest();
                    request.FilePath = arqvideo;
                    request.BucketName = bucket;
                    request.Key = directoryS3 + arqvideo.Substring(arqvideo.LastIndexOf("\\") + 1);
                    //request.CannedACL = S3CannedACL.PublicRead;
                    PutObjectResponse response = await s3Client.PutObjectAsync(request);
                    Log.Information("Video " + arqvideo.Substring(arqvideo.LastIndexOf("\\") + 1) + " enviado com sucesso");
                    if(!response.HttpStatusCode.Equals(HttpStatusCode.OK))
                    {
                        Log.Error("Video " + arqvideo.Substring(arqvideo.LastIndexOf("\\") + 1) + " não foi enviado para S3");
                    }
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}
