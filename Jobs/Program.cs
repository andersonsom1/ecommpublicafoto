﻿using Application;
using Domain;
using Domain.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog;
using Services;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Jobs
{
    class Program
    {
        public static IConfigurationRoot Configuration;
        public static async Task Main(string[] args)
        {
            var environment = "Production";

            var builder = new ConfigurationBuilder()
               .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
               .AddJsonFile($"{ environment }.json", optional: false, reloadOnChange: true);


            Configuration = builder.Build();
            var services = new ServiceCollection();

            services.AddSingleton<IVideo, Video>();
            services.AddSingleton<IServiceVideo, ServiceVideo>();
            //services.AddSingleton<IProductsRepository, ProductsRepository>();

            services.Configure<OracleJarvis>(Configuration.GetSection("OracleJarvis"));
            services.Configure<AppSettings>(Configuration.GetSection("ApiSettings"));

            services.AddOptions();

            var pathLog = Configuration.GetSection("PathLog").Value;

            Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console()
                    .WriteTo.File(pathLog + "PublicaVideo_-.txt", rollingInterval: RollingInterval.Day)
                    .CreateLogger();
            

            var serviceProvider = services.BuildServiceProvider();

            IVideo video = serviceProvider.GetService<IVideo>();

            //await stockBlockControl.ProcessStockBlockControlAsync();

            await video.ProcessarVideoAsync();
        }
    }
}
