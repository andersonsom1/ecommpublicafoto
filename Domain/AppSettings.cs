﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class AppSettings
    {
        //Url's
        public string Url { get; set; }
        public string UrlS3 { get; set; }
        public string Bucketnamevideo { get; set; }
        public string AccessKeyvideo { get; set; }
        public string SecretKey { get; set; }
        public string DirectoryS3 { get; set; }
        public string UrlHLogNet { get; set; }
        public string UrlOrderBroker { get; set; }
        public string UrlMarginStore { get; set; }
        public string UrlWebApiCrm { get; set; }
        public string UrlMarketData { get; set; }
        public string UrlEstoqueOnlineV2 { get; set; }
        public string UrlXmob { get; set; }
        public string UrlMercadoria { get; set; }
        public string UrlTagFacil { get; set; }
        public string UserTagFacil { get; set; }
        public string PwdTagFacil { get; set; }
        public string UrlTMS { get; set; }
        public string UserTMS { get; set; }
        public string PwdTMS { get; set; }
        public string EmailProvider { get; set; }
        public string TokenProvider { get; set; }
        public string UserTokenProvider { get; set; }
        public string PwdTokenProvider { get; set; }
        public string UrlRastreamento { get; set; }
        public string UrlNodesToken { get; set; }
        public string IdNodes { get; set; }
        public string PassNodes { get; set; }
        public string UrlLoggi { get; set; }
        public string LoggiAutorization { get; set; }
        public string UrlGeoLocation { get; set; }
        public string UrlSourceFiles { get; set; }
        //Parameters
        public string VtexKey { get; set; }
        public string VtexToken { get; set; }
        public string PasswordOrderBroker { get; set; }
        public string UserOrderBroker { get; set; }
        public string StockWareHouseId { get; set; }
        public int QttyAttemptsSendStockVtex { get; set; }
        public string UrlMercadoriaMiddleware { get; set; }
        public string UrlPushPluginOrderStatus { get; set; }
    }
}