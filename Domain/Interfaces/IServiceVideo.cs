﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IServiceVideo
    {
        List<string> CarregaArquivos();
        Task EnviarVideoAsync(List<string> videos);
        void DeletarArquivos(List<string> arquivos);
    }
}
